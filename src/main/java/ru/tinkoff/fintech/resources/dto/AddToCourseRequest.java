package ru.tinkoff.fintech.resources.dto;

import java.util.List;
import java.util.UUID;
import javax.validation.GroupSequence;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import ru.tinkoff.fintech.validation.ValidGrade;

import static ru.tinkoff.fintech.resources.dto.AddToCourseRequest.CustomGroup;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
@ValidGrade(groups = CustomGroup.class)
public class AddToCourseRequest {

    @NotNull
    UUID courseId;

    @NotEmpty
    List<UUID> studentIds;

    public interface CustomGroup {}

    @GroupSequence({Default.class, CustomGroup.class})
    public interface AddToCourseRequestValidationSequence {}
}
