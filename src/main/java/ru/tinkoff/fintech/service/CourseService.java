package ru.tinkoff.fintech.service;

import java.util.List;
import java.util.UUID;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.tinkoff.fintech.model.Course;
import ru.tinkoff.fintech.repository.CourseRepository;

@Service
@RequiredArgsConstructor
public class CourseService {

    private final CourseRepository repository;

    public void save(Course course) {
        repository.save(course);
    }

    public void update(Course course) {
        repository.update(course);
    }

    public List<Course> findAll(List<UUID> ids) {
        return repository.findAll(ids);
    }

    public void delete(UUID id) {
        repository.delete(id);
    }
}
