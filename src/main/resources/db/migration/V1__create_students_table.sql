CREATE TABLE students
(
    id    UUID PRIMARY KEY,
    name  VARCHAR(200) NOT NULL,
    age   SMALLINT     NOT NULL,
    grade SMALLINT     NOT NULL
);

CREATE TABLE courses
(
    id             UUID PRIMARY KEY,
    name           VARCHAR(200) NOT NULL,
    description    VARCHAR(800) NOT NULL,
    required_grade SMALLINT     NOT NULL
);

create table student_course
(
    cid UUID NOT NULL,
    sid UUID NOT NULL,
    FOREIGN KEY (cid) REFERENCES courses (id) ON DELETE CASCADE,
    FOREIGN KEY (sid) REFERENCES students (id) ON DELETE CASCADE,
    CONSTRAINT cid_sid_unique UNIQUE (cid, sid)
);